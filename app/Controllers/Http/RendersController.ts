import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import Env from '@ioc:Adonis/Core/Env'
import { schema } from '@ioc:Adonis/Core/Validator'
import Kube from '@ioc:Adonis/Addons/Kube'
import Render from 'App/Models/Render'
import Logger from '@ioc:Adonis/Core/Logger'
import path from 'path'

export default class RendersController {
  public async newRender({ request }: HttpContextContract) {
    const validated = await request.validate({
      schema: schema.create({
        title: schema.string(),
        blend: schema.file({ extnames: ['blend'] }),
        renderType: schema.enum(['single', 'animation'] as const),
        endFrame: schema.number(),
      }),
    })

    const { title, blend, renderType, endFrame } = validated

    const slug = title.toLowerCase().replace(/[^a-z0-9]+/g, '_')

    const render = new Render()
    render.title = title
    render.slug = slug
    render.endFrame = endFrame
    await render.save()

    await blend.move(Application.tmpPath('blends'), {
      name: `${slug}.${blend.extname}`,
      overwrite: true,
    })

    const baseUrl = Env.get('BASE_URL')

    const downloadURL = `${baseUrl}/blendfile/${slug}.blend`

    if (renderType === 'animation') {
      for (let i = 1; i < endFrame + 1; i++) {
        const uploadURL = `${baseUrl}/frame/${slug}/${i}`
        /* Launch job */
        const result = await Kube.createNamespacedJob('romain', {
          apiVersion: 'batch/v1',
          kind: 'Job',
          metadata: {
            name: `blender-render-${slug}-${i}`,
          },
          spec: {
            template: {
              spec: {
                containers: [
                  {
                    name: 'blender',
                    image: 'romch007/blender-render',
                    env: [
                      { name: 'FRAME', value: String(i) },
                      { name: 'DOWNLOAD_URL', value: downloadURL },
                      { name: 'UPLOAD_URL', value: uploadURL },
                    ],
                  },
                ],
                restartPolicy: 'Never',
              },
            },
            backoffLimit: 4,
          },
        }).catch((error) => Logger.error(error))
      }
    }
  }

  public async getBlendFile({ params, response }: HttpContextContract) {
    const slug: string = params.slug
    response.download(path.join(Application.tmpPath('blends'), `${slug}.blend`))
  }

  public async uploadFrame({ request, params }: HttpContextContract) {
    const validated = await request.validate({
      schema: schema.create({
        frame: schema.file({ extnames: ['png'] }),
      }),
    })
    const { slug, n } = params
    const { frame } = validated

    await frame.move(Application.tmpPath(`${slug}`), { name: `${n}.png` })
  }
}
