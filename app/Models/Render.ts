import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'
import { DateTime } from 'luxon'

export default class Render extends BaseModel {
  public static table = 'renders'

  @column({ isPrimary: true })
  public id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column()
  public slug: String

  @column()
  public title: String

  @column()
  public endFrame: number

  @column()
  public completedFrames: number
}
