FROM node:13 AS builder

WORKDIR /app

COPY package.json yarn.* ./

RUN yarn install

COPY . .

RUN node ace build

FROM node:13

ENV PORT=80
ENV HOST=0.0.0.0

WORKDIR /app

COPY --from=builder /app /app

EXPOSE 80

CMD ["node"]
