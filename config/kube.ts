import { KubeConfig } from '@ioc:Adonis/Addons/Kube'

const kubeConfig: KubeConfig = {
  from: 'custom',
}

export default kubeConfig
