declare module '@ioc:Adonis/Addons/Kube' {
  import k8s from '@kubernetes/client-node'

  export interface KubeConfig {
    from: 'internal' | 'custom'
  }

  const Kube: k8s.BatchV1Api
  export default Kube
}
