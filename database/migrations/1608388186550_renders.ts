import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Renders extends BaseSchema {
  protected tableName = 'renders'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('title').notNullable()
      table.string('slug').notNullable()
      table.integer('end_frame').notNullable()
      table.integer('completed_frames').notNullable().defaultTo(0)
      table.timestamps()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
